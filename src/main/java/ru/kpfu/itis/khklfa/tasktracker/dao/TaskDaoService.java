package ru.kpfu.itis.khklfa.tasktracker.dao;

import ru.kpfu.itis.khklfa.tasktracker.dto.model.SearchDto;
import ru.kpfu.itis.khklfa.tasktracker.entity.Task;
import ru.kpfu.itis.khklfa.tasktracker.entity.enums.TaskStatus;

import java.util.List;
import java.util.Set;
import java.util.UUID;

public interface TaskDaoService {
    Task createTask(Task task);

    Task findById(UUID taskId);

    List<Task> getUserTaskListByUserName(String userName);

    List<Task> getAll();

    List<Task> getUserTaskListByUserNameAndStatus(String userName, TaskStatus taskStatus);

    List<Task> getTaskListBySearchDtoAndTaskStatus(SearchDto searchDto, TaskStatus taskStatus);

    List<Task> getChildrenTasks(UUID taskId);

    List<Task> getUserTaskListByStatus(TaskStatus taskStatus);

    /**
     * Добавлено после презентации.
     * Доработка: Отображать в профиле пользователя созданные задания
     */
    List<Task> getUserTaskListByCreatedUserName(String userName);
}
