package ru.kpfu.itis.khklfa.tasktracker.security.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import ru.kpfu.itis.khklfa.tasktracker.dto.view.UserViewDto;
import ru.kpfu.itis.khklfa.tasktracker.entity.User;
import ru.kpfu.itis.khklfa.tasktracker.security.AuthDataHolder;

import java.util.Optional;

@Component
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class AuthDataHolderInSession implements AuthDataHolder {

    private UserViewDto user;

    @Override
    public String getUserName() {
        return Optional.ofNullable(user.getLogin()).orElse("");
    }

    @Override
    public UserViewDto getUser() {
        return user;
    }

    @Override
    public void setUser(UserViewDto user) {
        this.user = user;
    }

}
