package ru.kpfu.itis.khklfa.tasktracker.dao;

import ru.kpfu.itis.khklfa.tasktracker.dto.view.UserViewDto;
import ru.kpfu.itis.khklfa.tasktracker.entity.User;

import java.util.List;
import java.util.Set;

public interface UserDaoService {

    User findByLogin(String login);

    User save(User user);

    boolean existByLogin(String login);

    User getCurrentUser();

    List<User> getAllUsers();
}
