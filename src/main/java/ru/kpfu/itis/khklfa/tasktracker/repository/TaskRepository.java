package ru.kpfu.itis.khklfa.tasktracker.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import ru.kpfu.itis.khklfa.tasktracker.entity.Task;
import ru.kpfu.itis.khklfa.tasktracker.entity.enums.TaskStatus;

import java.util.List;
import java.util.UUID;

public interface TaskRepository extends JpaRepository<Task, UUID>, JpaSpecificationExecutor<Task> {
    List<Task> findAllByExecutor_Login(String userName);

    List<Task> findAllByExecutor_LoginAndTaskStatus(String userName, TaskStatus taskStatus);

    List<Task> findAllByParentId(UUID parentId);

    /**
     * Добавлено после презентации.
     * Доработка: Отображать в профиле пользователя созданные задания
     */
    List<Task> findAllByCreator_Login(String userName);
}
