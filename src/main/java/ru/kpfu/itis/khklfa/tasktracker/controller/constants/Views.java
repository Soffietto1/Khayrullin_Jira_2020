package ru.kpfu.itis.khklfa.tasktracker.controller.constants;

public interface Views {

    interface User {
        String REGISTER = "register";
        String LOGIN = "login";
        String PROFILE = "profile";
    }

    interface Task {
        String TASK_LIST = "task_list";
        String TASK_CREATE = "task_create";
        String TASK = "task";
    }
}
