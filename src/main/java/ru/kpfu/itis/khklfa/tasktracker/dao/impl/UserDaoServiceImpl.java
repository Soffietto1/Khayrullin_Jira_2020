package ru.kpfu.itis.khklfa.tasktracker.dao.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kpfu.itis.khklfa.tasktracker.dao.UserDaoService;
import ru.kpfu.itis.khklfa.tasktracker.dto.view.UserViewDto;
import ru.kpfu.itis.khklfa.tasktracker.entity.User;
import ru.kpfu.itis.khklfa.tasktracker.exceptions.NotFoundInDbException;
import ru.kpfu.itis.khklfa.tasktracker.repository.UserRepository;
import ru.kpfu.itis.khklfa.tasktracker.security.AuthDataHolder;

import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class UserDaoServiceImpl implements UserDaoService {

    private final UserRepository userRepository;
    private final AuthDataHolder authDataHolder;

    @Override
    @Transactional(readOnly = true)
    public User findByLogin(String login) {
        return userRepository.findByLogin(login)
                .orElseThrow(NotFoundInDbException::new);
    }

    @Override
    @Transactional
    public User save(User user) {
        return userRepository.save(user);
    }

    @Override
    @Transactional(readOnly = true)
    public boolean existByLogin(String login) {
        return userRepository.existsByLogin(login);
    }

    @Override
    @Transactional(readOnly = true)
    public User getCurrentUser() {
        return findByLogin(authDataHolder.getUserName());
    }

    @Override
    @Transactional(readOnly = true)
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }
}
