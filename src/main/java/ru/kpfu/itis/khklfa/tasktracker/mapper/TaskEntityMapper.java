package ru.kpfu.itis.khklfa.tasktracker.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import ru.kpfu.itis.khklfa.tasktracker.dto.model.TaskDto;
import ru.kpfu.itis.khklfa.tasktracker.dto.view.TaskViewDto;
import ru.kpfu.itis.khklfa.tasktracker.entity.Task;
import ru.kpfu.itis.khklfa.tasktracker.util.DateUtil;

import java.time.LocalDate;
import java.util.List;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        imports = {LocalDate.class, DateUtil.class})
public interface TaskEntityMapper {

    @Mapping(target = "deadlineDateTime", expression = "java(LocalDate.parse(taskDto.getDeadlineDateTime()))")
    Task toEntity(TaskDto taskDto);

    @Mapping(target = "isExpired", expression = "java(DateUtil.isExpired(task.getDeadlineDateTime()))")
    TaskViewDto toViewDto(Task task);

    List<TaskViewDto> toViewDto(List<Task> userTaskList);
}
