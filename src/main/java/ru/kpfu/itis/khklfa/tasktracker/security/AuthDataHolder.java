package ru.kpfu.itis.khklfa.tasktracker.security;

import ru.kpfu.itis.khklfa.tasktracker.dto.view.UserViewDto;
import ru.kpfu.itis.khklfa.tasktracker.entity.User;

public interface AuthDataHolder {

    String getUserName();

    UserViewDto getUser();

    void setUser(UserViewDto user);

}
