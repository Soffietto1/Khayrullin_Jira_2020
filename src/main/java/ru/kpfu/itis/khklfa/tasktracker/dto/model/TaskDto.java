package ru.kpfu.itis.khklfa.tasktracker.dto.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.kpfu.itis.khklfa.tasktracker.dto.view.UserViewDto;
import ru.kpfu.itis.khklfa.tasktracker.entity.enums.TaskPriority;
import ru.kpfu.itis.khklfa.tasktracker.entity.enums.TaskStatus;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TaskDto {
    private String name;
    private String description;
    private String acceptanceCriterion;
    private TaskStatus taskStatus;
    private TaskPriority priority;
    private String executorLogin;
    private String deadlineDateTime;
    private UUID parentTaskId;
}
