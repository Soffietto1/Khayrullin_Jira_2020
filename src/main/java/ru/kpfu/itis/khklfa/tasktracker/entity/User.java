package ru.kpfu.itis.khklfa.tasktracker.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.kpfu.itis.khklfa.tasktracker.entity.enums.Role;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Table;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "users")
@Accessors(chain = true)
public class User extends AbstractEntity {

    @Column(name = "name")
    private String name;

    @Column(name = "login")
    private String login;

    @Column(name = "password")
    private char[] password;

    @Column(name = "role")
    @ElementCollection(fetch = FetchType.EAGER)
    @Enumerated(EnumType.STRING)
    private Set<Role> roles;

}
