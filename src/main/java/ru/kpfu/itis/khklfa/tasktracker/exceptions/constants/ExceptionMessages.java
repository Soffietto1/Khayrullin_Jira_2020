package ru.kpfu.itis.khklfa.tasktracker.exceptions.constants;

public interface ExceptionMessages {

    String NOT_FOUND_IN_DB = "Entity not found";
}
