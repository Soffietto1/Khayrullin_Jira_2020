package ru.kpfu.itis.khklfa.tasktracker.entity.enums;

import java.util.Arrays;

public enum TaskPriority {
    LOW,
    MIDDLE,
    HIGH;

    public static TaskPriority[] getAllExcept(TaskPriority taskStatus) {
        return Arrays.stream(values())
                .filter(taskStatus1 -> taskStatus1 != taskStatus)
                .toArray(TaskPriority[]::new);
    }
}
