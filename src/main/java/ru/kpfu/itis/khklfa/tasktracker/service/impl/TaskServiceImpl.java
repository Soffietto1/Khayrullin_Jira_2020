package ru.kpfu.itis.khklfa.tasktracker.service.impl;

import liquibase.util.StringUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.khklfa.tasktracker.dao.TaskDaoService;
import ru.kpfu.itis.khklfa.tasktracker.dao.UserDaoService;
import ru.kpfu.itis.khklfa.tasktracker.dto.model.SearchDto;
import ru.kpfu.itis.khklfa.tasktracker.dto.model.TaskDto;
import ru.kpfu.itis.khklfa.tasktracker.dto.view.TaskViewDto;
import ru.kpfu.itis.khklfa.tasktracker.entity.Task;
import ru.kpfu.itis.khklfa.tasktracker.entity.User;
import ru.kpfu.itis.khklfa.tasktracker.entity.enums.TaskStatus;
import ru.kpfu.itis.khklfa.tasktracker.mapper.TaskEntityMapper;
import ru.kpfu.itis.khklfa.tasktracker.security.AuthDataHolder;
import ru.kpfu.itis.khklfa.tasktracker.service.TaskService;
import ru.kpfu.itis.khklfa.tasktracker.util.DateUtil;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TaskServiceImpl implements TaskService {

    private final TaskDaoService taskDaoService;
    private final UserDaoService userDaoService;
    private final TaskValidationService taskValidationService;
    private final TaskEntityMapper taskEntityMapper;
    private final AuthDataHolder authDataHolder;

    @Override
    public TaskViewDto create(TaskDto taskDto) {
        taskValidationService.validateCreate(taskDto);

        Task task = taskEntityMapper.toEntity(taskDto);
        task.setCreateDate(DateUtil.getCurrentDateTime());
        task.setCreator(userDaoService.getCurrentUser());
        task.setExecutor(userDaoService.findByLogin(taskDto.getExecutorLogin()));
        task.setTaskStatus(TaskStatus.TO_DO);

        Task parentTask = null;
        if (taskDto.getParentTaskId() != null) {
            parentTask = taskDaoService.findById(taskDto.getParentTaskId());
            task.setParent(parentTask);
        }

        Task savedTask = taskDaoService.createTask(task);
        if (parentTask != null) {
            parentTask.setIsParent(true);
            taskDaoService.createTask(parentTask);
        }
        return taskEntityMapper.toViewDto(savedTask);
    }

    @Override
    public List<TaskViewDto> getCurrentUserTaskList() {
        List<Task> userTaskList = taskDaoService.getUserTaskListByUserName(authDataHolder.getUserName());
        return taskEntityMapper.toViewDto(userTaskList);
    }

    @Override
    public List<TaskViewDto> getTaskListWithStatus(TaskStatus taskStatus) {
        List<Task> userTaskList = taskDaoService.getUserTaskListByStatus(taskStatus);
        return taskEntityMapper.toViewDto(userTaskList);
    }

    @Override
    public List<TaskViewDto> getCurrentUserTaskListWithStatus(TaskStatus taskStatus) {
        List<Task> userTaskList = taskDaoService.getUserTaskListByUserNameAndStatus(authDataHolder.getUserName(), taskStatus);
        return taskEntityMapper.toViewDto(userTaskList);
    }

    @Override
    public List<TaskViewDto> getCurrentUserTaskListWithStatus(SearchDto searchDto, TaskStatus taskStatus) {
        List<Task> userTaskList = taskDaoService.getTaskListBySearchDtoAndTaskStatus(searchDto, taskStatus);
        return taskEntityMapper.toViewDto(userTaskList);
    }

    @Override
    public TaskViewDto getTask(UUID taskId) {
        Task task = taskDaoService.findById(taskId);
        return taskEntityMapper.toViewDto(task);
    }

    @Override
    public List<TaskViewDto> getAll() {
        List<Task> tasks = taskDaoService.getAll();
        return taskEntityMapper.toViewDto(tasks);
    }

    @Override
    public TaskViewDto update(UUID taskId, TaskDto taskDto) {
        Task task = taskDaoService.findById(taskId);

         //Добавлено после презентации.
         //Доработка: Родительская задача не может быть DONE или FAILED, если дочерние задачи не выполнены
        taskValidationService.validateUpdate(taskDto, task);

        checkAndSet(taskDto.getName(), task::setName);
        checkAndSet(taskDto.getAcceptanceCriterion(), task::setAcceptanceCriterion);
        checkAndSet(taskDto.getDescription(), task::setDescription);
        checkAndSet(taskDto.getPriority(), task::setPriority);
        checkAndSet(taskDto.getDeadlineDateTime(), (deadline) -> {
            if (StringUtils.isNotEmpty(deadline)) {
                task.setDeadlineDateTime(LocalDate.parse(deadline));
            }
        });
        checkAndSet(taskDto.getTaskStatus(), task::setTaskStatus);
        checkAndSet(taskDto.getExecutorLogin(), (executorLogin) -> {
            User user = userDaoService.findByLogin(executorLogin);
            task.setExecutor(user);
        });
        checkAndSet(taskDto.getParentTaskId(), (parentId) -> {
            Task parent = taskDaoService.findById(parentId);
            task.setParent(parent);
        });
        task.setUpdateDate(DateUtil.getCurrentDateTime());

        Task savedTask = taskDaoService.createTask(task);

        return taskEntityMapper.toViewDto(savedTask);
    }

    @Override
    public List<TaskViewDto> getChildrenTask(UUID taskId) {
        List<Task> tasks = taskDaoService.getChildrenTasks(taskId);
        return taskEntityMapper.toViewDto(tasks);
    }

    @Override
    public List<TaskViewDto> getAllExcept(TaskViewDto taskViewDto) {
        List<Task> all = taskDaoService.getAll().stream()
                .filter(task -> !task.getId().equals(taskViewDto.getId()))
                .collect(Collectors.toList());
        return taskEntityMapper.toViewDto(all);
    }

    /**
     * Добавлено после презентации.
     * Доработка: Отображать в профиле пользователя созданные задания
     */
    @Override
    public List<TaskViewDto> getCurrentUserCreatedTaskList() {
        List<Task> all = taskDaoService.getUserTaskListByCreatedUserName(authDataHolder.getUserName());
        return taskEntityMapper.toViewDto(all);
    }

    private <T> void checkAndSet(T fieldValue, Consumer<T> fieldSetter) {
        if (fieldValue != null) {
            fieldSetter.accept(fieldValue);
        }
    }
}
