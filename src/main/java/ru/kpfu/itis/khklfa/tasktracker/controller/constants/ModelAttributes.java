package ru.kpfu.itis.khklfa.tasktracker.controller.constants;

public interface ModelAttributes {

    String ERROR = "error";

    interface User {
        String LOGIN_FORM = "login_form";
        String USER = "user";
        String ALL_USERS = "all_users";
    }

    interface Tasks {
        String CURRENT_USER_TASKS_TODO = "current_user_tasks_todo";
        String CURRENT_USER_TASKS_IN_PROGRESS = "current_user_tasks_in_progress";
        String CURRENT_USER_TASKS_DONE = "current_user_tasks_done";
        String CURRENT_USER_TASKS_IN_PROGRESS_FAIL = "current_user_tasks_in_progress_fail";
        String CURRENT_USER_TASKS_FAILED = "current_user_tasks_failed";
        String CURRENT_USER_TASKS_WAITING = "current_user_tasks_waiting";
        String TASK = "task";
        String TASK_UPDATE = "task_update_form";
        String ALL_TASKS = "all_tasks";
        String CREATED_TASKS = "created_tasks";
        String ALL_PRIORITIES = "all_priorities";
        String ALL_STATUSES = "all_statuses";
        String CHILDREN = "children";
        String SEARCH = "search";
        String CHILD_ERROR = "child_error";
    }

}
