package ru.kpfu.itis.khklfa.tasktracker.entity.enums;

import java.util.Arrays;

public enum TaskStatus {
    TO_DO,
    IN_PROGRESS,
    WAITING,
    DONE,
    IN_PROGRESS_EXPIRED,
    DONE_EXPIRED;

    public static TaskStatus[] getAllExcept(TaskStatus taskStatus) {
        return Arrays.stream(values())
                .filter(taskStatus1 -> taskStatus1 != taskStatus)
                .toArray(TaskStatus[]::new);
    }
}
