package ru.kpfu.itis.khklfa.tasktracker.service;

import ru.kpfu.itis.khklfa.tasktracker.dto.model.SearchDto;
import ru.kpfu.itis.khklfa.tasktracker.dto.model.TaskDto;
import ru.kpfu.itis.khklfa.tasktracker.dto.view.TaskViewDto;
import ru.kpfu.itis.khklfa.tasktracker.entity.enums.TaskStatus;

import java.util.List;
import java.util.Set;
import java.util.UUID;

public interface TaskService {

    TaskViewDto create(TaskDto taskDto);

    List<TaskViewDto> getCurrentUserTaskList();

    List<TaskViewDto> getTaskListWithStatus(TaskStatus taskStatus);

    List<TaskViewDto> getCurrentUserTaskListWithStatus(TaskStatus taskStatus);

    List<TaskViewDto> getCurrentUserTaskListWithStatus(SearchDto searchDto, TaskStatus taskStatus);

    TaskViewDto getTask(UUID taskId);

    List<TaskViewDto> getAll();

    TaskViewDto update(UUID taskId, TaskDto taskDto);

    List<TaskViewDto> getChildrenTask(UUID taskId);

    List<TaskViewDto> getAllExcept(TaskViewDto task);

    /**
     * Добавлено после презентации.
     * Доработка: Отображать в профиле пользователя созданные задания
     */
    List<TaskViewDto> getCurrentUserCreatedTaskList();
}
