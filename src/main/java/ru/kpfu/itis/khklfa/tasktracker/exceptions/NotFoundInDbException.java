package ru.kpfu.itis.khklfa.tasktracker.exceptions;

import ru.kpfu.itis.khklfa.tasktracker.exceptions.constants.ExceptionMessages;

public class NotFoundInDbException extends RuntimeException {

    public NotFoundInDbException() {
        super(ExceptionMessages.NOT_FOUND_IN_DB);
    }

}
