package ru.kpfu.itis.khklfa.tasktracker.dto.view;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.kpfu.itis.khklfa.tasktracker.entity.enums.TaskPriority;
import ru.kpfu.itis.khklfa.tasktracker.entity.enums.TaskStatus;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TaskViewDto {

    private UUID id;
    private String name;
    private String description;
    private String acceptanceCriterion;
    private TaskPriority priority;
    private TaskStatus taskStatus;
    private UserViewDto creator;
    private UserViewDto executor;
    private LocalDate deadlineDateTime;
    private LocalDateTime createDate;
    private LocalDateTime updateDate;
    private TaskViewDto parent;
    private Boolean isParent;
    private Boolean isExpired;
}
