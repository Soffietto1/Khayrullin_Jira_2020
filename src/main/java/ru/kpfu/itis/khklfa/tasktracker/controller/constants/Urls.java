package ru.kpfu.itis.khklfa.tasktracker.controller.constants;

public interface Urls {

    interface User {
        String LOGIN = "/login";
        String REGISTER = "/register";
        String LOGOUT = "/logout";
        String PROFILE = "/profile";
    }

    interface Task {
        String TASK_LIST = "/task_list";
        String TASK = "/task";
        String TASK_ID = "/{task_id}";
        String TASK_WITH_ID = TASK + TASK_ID;
        String TASK_CREATE = TASK + "/create";
        String TASK_UPDATE = TASK + "/update";
        String TASK_UPDATE_ID = TASK_UPDATE + TASK_ID;
        String SEARCH = TASK_LIST + "/search";
    }
}
