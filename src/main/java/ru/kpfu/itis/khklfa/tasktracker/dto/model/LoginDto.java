package ru.kpfu.itis.khklfa.tasktracker.dto.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginDto {
    private String login;
    private String password;
}
