package ru.kpfu.itis.khklfa.tasktracker.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.kpfu.itis.khklfa.tasktracker.controller.constants.ModelAttributes;
import ru.kpfu.itis.khklfa.tasktracker.controller.constants.Params;
import ru.kpfu.itis.khklfa.tasktracker.controller.constants.Urls;
import ru.kpfu.itis.khklfa.tasktracker.controller.constants.Views;
import ru.kpfu.itis.khklfa.tasktracker.dto.model.SearchDto;
import ru.kpfu.itis.khklfa.tasktracker.dto.model.TaskDto;
import ru.kpfu.itis.khklfa.tasktracker.dto.view.TaskViewDto;
import ru.kpfu.itis.khklfa.tasktracker.dto.view.UserViewDto;
import ru.kpfu.itis.khklfa.tasktracker.entity.enums.TaskPriority;
import ru.kpfu.itis.khklfa.tasktracker.entity.enums.TaskStatus;
import ru.kpfu.itis.khklfa.tasktracker.exceptions.ValidationException;
import ru.kpfu.itis.khklfa.tasktracker.service.TaskService;
import ru.kpfu.itis.khklfa.tasktracker.service.UserService;

import java.util.List;
import java.util.UUID;

@Controller
@RequiredArgsConstructor
public class TaskController {

    private final TaskService taskService;
    private final UserService userService;

    @GetMapping(Urls.Task.TASK_LIST)
    public String getTaskList(ModelMap model) {
        List<TaskViewDto> toDoTasks = taskService.getTaskListWithStatus(TaskStatus.TO_DO);
        model.addAttribute(ModelAttributes.Tasks.CURRENT_USER_TASKS_TODO, toDoTasks);

        List<TaskViewDto> inProgressTasks = taskService.getTaskListWithStatus(TaskStatus.IN_PROGRESS);
        model.addAttribute(ModelAttributes.Tasks.CURRENT_USER_TASKS_IN_PROGRESS, inProgressTasks);

        List<TaskViewDto> doneTasks = taskService.getTaskListWithStatus(TaskStatus.DONE);
        model.addAttribute(ModelAttributes.Tasks.CURRENT_USER_TASKS_DONE, doneTasks);

        List<TaskViewDto> inProgressFailTasks = taskService.getTaskListWithStatus(TaskStatus.IN_PROGRESS_EXPIRED);
        model.addAttribute(ModelAttributes.Tasks.CURRENT_USER_TASKS_IN_PROGRESS_FAIL, inProgressFailTasks);

        List<TaskViewDto> failedTasks = taskService.getTaskListWithStatus(TaskStatus.DONE_EXPIRED);
        model.addAttribute(ModelAttributes.Tasks.CURRENT_USER_TASKS_FAILED, failedTasks);

        List<TaskViewDto> waitingTasks = taskService.getTaskListWithStatus(TaskStatus.WAITING);
        model.addAttribute(ModelAttributes.Tasks.CURRENT_USER_TASKS_WAITING, waitingTasks);

        List<UserViewDto> allUsers = userService.getAll();
        model.addAttribute(ModelAttributes.User.ALL_USERS, allUsers);

        model.addAttribute(ModelAttributes.Tasks.SEARCH, new SearchDto());

        return Views.Task.TASK_LIST;
    }

    @GetMapping(Urls.Task.SEARCH)
    public String getSearchTaskList(
            @ModelAttribute(ModelAttributes.Tasks.SEARCH) SearchDto searchDto,
            ModelMap model) {
        List<TaskViewDto> toDoTasks = taskService.getCurrentUserTaskListWithStatus(searchDto, TaskStatus.TO_DO);
        model.addAttribute(ModelAttributes.Tasks.CURRENT_USER_TASKS_TODO, toDoTasks);

        List<TaskViewDto> inProgressTasks = taskService.getCurrentUserTaskListWithStatus(searchDto, TaskStatus.IN_PROGRESS);
        model.addAttribute(ModelAttributes.Tasks.CURRENT_USER_TASKS_IN_PROGRESS, inProgressTasks);

        List<TaskViewDto> doneTasks = taskService.getCurrentUserTaskListWithStatus(searchDto, TaskStatus.DONE);
        model.addAttribute(ModelAttributes.Tasks.CURRENT_USER_TASKS_DONE, doneTasks);

        List<TaskViewDto> inProgressFailTasks = taskService.getCurrentUserTaskListWithStatus(searchDto, TaskStatus.IN_PROGRESS_EXPIRED);
        model.addAttribute(ModelAttributes.Tasks.CURRENT_USER_TASKS_IN_PROGRESS_FAIL, inProgressFailTasks);

        List<TaskViewDto> failedTasks = taskService.getCurrentUserTaskListWithStatus(searchDto, TaskStatus.DONE_EXPIRED);
        model.addAttribute(ModelAttributes.Tasks.CURRENT_USER_TASKS_FAILED, failedTasks);

        List<TaskViewDto> waitingTasks = taskService.getCurrentUserTaskListWithStatus(searchDto, TaskStatus.WAITING);
        model.addAttribute(ModelAttributes.Tasks.CURRENT_USER_TASKS_WAITING, waitingTasks);

        List<UserViewDto> allUsers = userService.getAll();
        model.addAttribute(ModelAttributes.User.ALL_USERS, allUsers);

        model.addAttribute(ModelAttributes.Tasks.SEARCH, new SearchDto());

        return Views.Task.TASK_LIST;
    }

    @GetMapping(Urls.Task.TASK_CREATE)
    public String getCreateTask(ModelMap modelMap) {
        modelMap.addAttribute(ModelAttributes.Tasks.TASK, new TaskDto());

        List<UserViewDto> allUsers = userService.getAll();
        List<TaskViewDto> allTasks = taskService.getAll();

        modelMap.addAttribute(ModelAttributes.Tasks.ALL_TASKS, allTasks);
        modelMap.addAttribute(ModelAttributes.User.ALL_USERS, allUsers);
        modelMap.addAttribute(ModelAttributes.Tasks.ALL_PRIORITIES, TaskPriority.values());
        return Views.Task.TASK_CREATE;
    }

    @GetMapping(Urls.Task.TASK_WITH_ID)
    public String getTask(@PathVariable(value = Params.Task.TASK_ID) UUID taskId,
                          ModelMap model) {
        TaskViewDto task = taskService.getTask(taskId);
        List<UserViewDto> allUsers = userService.getAllExceptCurrent();
        List<TaskViewDto> allTasks = taskService.getAllExcept(task);
        List<TaskViewDto> childrenTasks = taskService.getChildrenTask(taskId);

        model.addAttribute(ModelAttributes.Tasks.CHILDREN, childrenTasks);
        model.addAttribute(ModelAttributes.Tasks.ALL_PRIORITIES, TaskPriority.getAllExcept(task.getPriority()));
        model.addAttribute(ModelAttributes.Tasks.ALL_STATUSES, TaskStatus.getAllExcept(task.getTaskStatus()));
        model.addAttribute(ModelAttributes.Tasks.ALL_TASKS, allTasks);
        model.addAttribute(ModelAttributes.User.ALL_USERS, allUsers);
        model.addAttribute(ModelAttributes.Tasks.TASK_UPDATE, new TaskDto());
        model.addAttribute(ModelAttributes.Tasks.TASK, task);
        return Views.Task.TASK;
    }

    @PostMapping(Urls.Task.TASK_CREATE)
    public String createTask(@ModelAttribute(ModelAttributes.Tasks.TASK) TaskDto taskDto,
                             ModelMap model) {
        TaskViewDto task = taskService.create(taskDto);
        List<UserViewDto> allUsers = userService.getAll();
        List<TaskViewDto> allTasks = taskService.getAll();
        List<TaskViewDto> childrenTasks = taskService.getChildrenTask(task.getId());

        model.addAttribute(ModelAttributes.Tasks.CHILDREN, childrenTasks);
        model.addAttribute(ModelAttributes.Tasks.ALL_TASKS, allTasks);
        model.addAttribute(ModelAttributes.User.ALL_USERS, allUsers);
        model.addAttribute(ModelAttributes.Tasks.ALL_PRIORITIES, TaskPriority.values());
        model.addAttribute(ModelAttributes.Tasks.ALL_STATUSES, TaskStatus.values());
        model.addAttribute(ModelAttributes.Tasks.TASK_UPDATE, new TaskDto());
        model.addAttribute(ModelAttributes.Tasks.TASK, task);
        return Views.Task.TASK;
    }

    @PostMapping(Urls.Task.TASK_UPDATE_ID)
    public String updateTask(@ModelAttribute(ModelAttributes.Tasks.TASK) TaskDto taskDto,
                             @PathVariable(Params.Task.TASK_ID) UUID taskId,
                             BindingResult bindingResult,
                             ModelMap model) {
        TaskViewDto task = null;
        try {
             task = taskService.update(taskId, taskDto);
        } catch (ValidationException ex) {
            task = taskService.getTask(taskId);
            model.addAttribute(ModelAttributes.Tasks.CHILD_ERROR, ex.getMessage());
        }
        List<UserViewDto> allUsers = userService.getAllExceptCurrent();
        List<TaskViewDto> allTasks = taskService.getAllExcept(task);
        List<TaskViewDto> childrenTasks = taskService.getChildrenTask(taskId);

        model.addAttribute(ModelAttributes.Tasks.CHILDREN, childrenTasks);
        model.addAttribute(ModelAttributes.Tasks.ALL_TASKS, allTasks);
        model.addAttribute(ModelAttributes.User.ALL_USERS, allUsers);
        model.addAttribute(ModelAttributes.Tasks.ALL_PRIORITIES, TaskPriority.getAllExcept(task.getPriority()));
        model.addAttribute(ModelAttributes.Tasks.ALL_STATUSES, TaskStatus.getAllExcept(task.getTaskStatus()));
        model.addAttribute(ModelAttributes.Tasks.TASK_UPDATE, new TaskDto());
        model.addAttribute(ModelAttributes.Tasks.TASK, task);
        return Views.Task.TASK;
    }
}
