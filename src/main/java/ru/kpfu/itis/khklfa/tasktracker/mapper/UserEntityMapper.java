package ru.kpfu.itis.khklfa.tasktracker.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;
import ru.kpfu.itis.khklfa.tasktracker.dto.model.RegisterDto;
import ru.kpfu.itis.khklfa.tasktracker.dto.view.UserViewDto;
import ru.kpfu.itis.khklfa.tasktracker.entity.User;

import java.util.List;


@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        imports = {Argon2PasswordEncoder.class})
public interface UserEntityMapper {

    @Mapping(target = "password", expression = "java(new Argon2PasswordEncoder().encode(registerDto.getPassword()).toCharArray())")
    User toEntity(RegisterDto registerDto);

    UserViewDto toViewDto(User user);

    List<UserViewDto> toViewDto(List<User> users);
}
