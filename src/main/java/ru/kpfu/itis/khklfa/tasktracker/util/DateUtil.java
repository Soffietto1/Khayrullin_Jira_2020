package ru.kpfu.itis.khklfa.tasktracker.util;

import lombok.experimental.UtilityClass;
import ru.kpfu.itis.khklfa.tasktracker.util.constants.TimeZone;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;

@UtilityClass
public class DateUtil {

    public LocalDateTime getCurrentDateTime() {
        return LocalDateTime.now(ZoneId.of(TimeZone.MSK));
    }

    public Boolean isExpired(LocalDate deadlineDateTime) {
        if (deadlineDateTime == null) {
            return false;
        }
        return deadlineDateTime.isBefore(getCurrentDateTime().toLocalDate());
    }
}
