package ru.kpfu.itis.khklfa.tasktracker.spec;

import lombok.experimental.UtilityClass;
import org.springframework.data.jpa.domain.Specification;
import ru.kpfu.itis.khklfa.tasktracker.entity.Task;
import ru.kpfu.itis.khklfa.tasktracker.entity.enums.TaskStatus;

import java.time.LocalDate;

@UtilityClass
public class TaskSpec {

    public Specification<Task> getExecutorSpec(String executorLogin) {
        return (task, cq, cb) -> cb.equal(task.join("executor").get("login"), executorLogin);
    }

    public Specification<Task> getDeadlineSpec(LocalDate from, LocalDate to) {
        return (task, cq, cb) -> cb.and(
                cb.greaterThanOrEqualTo(task.get("deadlineDateTime"), from),
                cb.lessThanOrEqualTo(task.get("deadlineDateTime"), to)
        );
    }

    public Specification<Task> getTaskStatusSpec(TaskStatus taskStatus) {
        return (task, cq, cb) -> cb.equal(task.get("taskStatus").as(TaskStatus.class), taskStatus);
    }
}
