package ru.kpfu.itis.khklfa.tasktracker.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.kpfu.itis.khklfa.tasktracker.entity.enums.TaskPriority;
import ru.kpfu.itis.khklfa.tasktracker.entity.enums.TaskStatus;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@Table(name = "tasks")
@Accessors(chain = true)
public class Task extends AbstractEntity {

    @Column(name = "name")
    private String name;

    @ManyToOne
    @JoinColumn(name = "parent")
    private Task parent;

    @Column(name = "description")
    private String description;

    @Column(name = "acceptance_criterion")
    private String acceptanceCriterion;

    @Column(name = "priority")
    @Enumerated(EnumType.STRING)
    private TaskPriority priority;

    @Column(name = "task_status")
    @Enumerated(EnumType.STRING)
    private TaskStatus taskStatus;

    @ManyToOne
    @JoinColumn(name = "creator")
    private User creator;

    @ManyToOne
    @JoinColumn(name = "executor")
    private User executor;

    @Column(name = "deadline_date_time")
    private LocalDate deadlineDateTime;

    @Column(name = "create_date")
    private LocalDateTime createDate;

    @Column(name = "update_date")
    private LocalDateTime updateDate;

    @Column(name = "is_parent")
    private Boolean isParent;

}
