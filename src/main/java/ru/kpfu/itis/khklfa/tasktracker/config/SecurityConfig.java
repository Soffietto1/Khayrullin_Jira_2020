package ru.kpfu.itis.khklfa.tasktracker.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import ru.kpfu.itis.khklfa.tasktracker.controller.constants.Urls;
import ru.kpfu.itis.khklfa.tasktracker.entity.enums.Role;
import ru.kpfu.itis.khklfa.tasktracker.service.UserService;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final UserService userService;
    private final AuthenticationSuccessHandler authenticationSuccessHandler;

    @Override
    protected void configure(HttpSecurity security) throws Exception {
        security.authorizeRequests()
                .antMatchers("/css/**").permitAll()
                .antMatchers("/").denyAll()
                .antMatchers(Urls.User.LOGIN, Urls.User.REGISTER).anonymous()
                .antMatchers(
                        Urls.Task.TASK + "/**",
                        Urls.Task.TASK_UPDATE + "/**",
                        Urls.Task.TASK_CREATE,
                        Urls.Task.TASK_LIST,
                        Urls.Task.SEARCH,
                        Urls.Task.TASK_UPDATE_ID,
                        Urls.User.PROFILE,
                        Urls.User.LOGOUT).hasAuthority(Role.USER.toString());

        security.csrf().disable()
                .formLogin()
                .loginPage(Urls.User.LOGIN)
                .usernameParameter("login")
                .defaultSuccessUrl(Urls.Task.TASK_LIST, true)
                .failureUrl(Urls.User.LOGIN + "?error=true")
                .successHandler(authenticationSuccessHandler)
                .and()
                .logout()
                .logoutUrl(Urls.User.LOGOUT)
                .logoutSuccessUrl(Urls.User.LOGIN).deleteCookies("JSESSIONID")
                .and()
                .rememberMe().key("uniqueAndSecret").rememberMeParameter("remember-me-new")
                .and()
                .exceptionHandling();
    }

    @Override
    protected UserDetailsService userDetailsService() {
        return userService;
    }

    @Bean
    public static PasswordEncoder passwordEncoder() {
        return new Argon2PasswordEncoder();
    }
}