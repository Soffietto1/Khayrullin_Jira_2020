package ru.kpfu.itis.khklfa.tasktracker.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.khklfa.tasktracker.dao.TaskDaoService;
import ru.kpfu.itis.khklfa.tasktracker.dto.model.TaskDto;
import ru.kpfu.itis.khklfa.tasktracker.entity.Task;
import ru.kpfu.itis.khklfa.tasktracker.entity.enums.TaskStatus;
import ru.kpfu.itis.khklfa.tasktracker.exceptions.ValidationException;

import java.util.List;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class TaskValidationService {

    private final TaskDaoService taskDaoService;

    public void validateCreate(TaskDto taskDto) {

    }

    /**
     * Добавлено после презентации.
     * Доработка: Родительская задача не может быть DONE или FAILED, если дочерние задачи не выполнены
     */
    public void validateUpdate(TaskDto taskDto, Task task) {
        validateDoneStatus(taskDto, task);
    }

    /**
     * Добавлено после презентации.
     * Доработка: Родительская задача не может быть DONE или FAILED, если дочерние задачи не выполнены
     */
    private void validateDoneStatus(TaskDto taskDto, Task task) {
        if (taskDto.getTaskStatus() == TaskStatus.DONE || taskDto.getTaskStatus() == TaskStatus.DONE_EXPIRED) {
            List<Task> childrenTasks = taskDaoService.getChildrenTasks(task.getId());
            boolean areAnyChildrenNotDoneOrFailed = childrenTasks.stream()
                    .filter(Objects::nonNull)
                    .anyMatch(
                            child -> child.getTaskStatus() != TaskStatus.DONE
                                    && child.getTaskStatus() != TaskStatus.DONE_EXPIRED
                    );
            if (areAnyChildrenNotDoneOrFailed) {
                throw new ValidationException("Задача не может быть завершена, пока все ее подзадачи не будут выполнены");
            }
        }
    }
}
