package ru.kpfu.itis.khklfa.tasktracker.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.kpfu.itis.khklfa.tasktracker.controller.constants.ModelAttributes;
import ru.kpfu.itis.khklfa.tasktracker.controller.constants.Urls;
import ru.kpfu.itis.khklfa.tasktracker.controller.constants.Views;
import ru.kpfu.itis.khklfa.tasktracker.dto.model.LoginDto;
import ru.kpfu.itis.khklfa.tasktracker.dto.model.RegisterDto;
import ru.kpfu.itis.khklfa.tasktracker.dto.view.TaskViewDto;
import ru.kpfu.itis.khklfa.tasktracker.dto.view.UserViewDto;
import ru.kpfu.itis.khklfa.tasktracker.service.TaskService;
import ru.kpfu.itis.khklfa.tasktracker.service.UserService;

import java.util.List;

import static ru.kpfu.itis.khklfa.tasktracker.util.UrlUtil.redirect;

@Controller
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;
    private final TaskService taskService;

    @GetMapping(Urls.User.LOGIN)
    public String login(@RequestParam(value = ModelAttributes.ERROR, required = false) boolean error, Model model) {
        model.addAttribute(ModelAttributes.ERROR, error);
        model.addAttribute(ModelAttributes.User.LOGIN_FORM, new LoginDto());
        return Views.User.LOGIN;
    }

    @GetMapping(Urls.User.REGISTER)
    public String register(Model model) {
        model.addAttribute(ModelAttributes.User.USER, new RegisterDto());
        return Views.User.REGISTER;
    }

    @PostMapping(Urls.User.REGISTER)
    public String register(@ModelAttribute(ModelAttributes.User.USER) RegisterDto registerDto, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return Views.User.REGISTER;
        }
        userService.register(registerDto);
        return redirect(Urls.Task.TASK_LIST);
    }

    @GetMapping(Urls.User.PROFILE)
    public String profile(ModelMap model) {
        UserViewDto userViewDto = userService.getAuthenticatedUser();
        List<TaskViewDto> currentUserTaskList = taskService.getCurrentUserTaskList();

        //Добавлено после презентации.
        //Доработка: Отображать в профиле пользователя созданные задания
        List<TaskViewDto> createdTaskList = taskService.getCurrentUserCreatedTaskList();

        model.addAttribute(ModelAttributes.Tasks.ALL_TASKS, currentUserTaskList);

        //Добавлено после презентации.
        //Доработка: Отображать в профиле пользователя созданные задания
        model.addAttribute(ModelAttributes.Tasks.CREATED_TASKS, createdTaskList);

        model.addAttribute(ModelAttributes.User.USER, userViewDto);
        return Views.User.PROFILE;
    }
}
