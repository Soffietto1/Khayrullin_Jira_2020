package ru.kpfu.itis.khklfa.tasktracker.service;

import org.springframework.security.core.userdetails.UserDetailsService;
import ru.kpfu.itis.khklfa.tasktracker.dto.view.UserViewDto;
import ru.kpfu.itis.khklfa.tasktracker.dto.model.RegisterDto;

import java.util.List;
import java.util.Set;

public interface UserService extends UserDetailsService {

    void register(RegisterDto registerDto);

    UserViewDto getAuthenticatedUser();

    List<UserViewDto> getAll();

    List<UserViewDto> getAllExceptCurrent();
}
