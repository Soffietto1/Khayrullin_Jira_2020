package ru.kpfu.itis.khklfa.tasktracker.exceptions;

/**
 * Добавлено после презентации.
 * Доработка: Родительская задача не может быть DONE или FAILED, если дочерние задачи не выполнены
 */
public class ValidationException extends RuntimeException {

    public ValidationException(String message) {
        super(message);
    }
}
