package ru.kpfu.itis.khklfa.tasktracker.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.khklfa.tasktracker.dao.UserDaoService;
import ru.kpfu.itis.khklfa.tasktracker.dto.model.RegisterDto;
import ru.kpfu.itis.khklfa.tasktracker.dto.view.UserViewDto;
import ru.kpfu.itis.khklfa.tasktracker.entity.User;
import ru.kpfu.itis.khklfa.tasktracker.entity.enums.Role;
import ru.kpfu.itis.khklfa.tasktracker.mapper.UserEntityMapper;
import ru.kpfu.itis.khklfa.tasktracker.security.impl.AuthDataHolderInSession;
import ru.kpfu.itis.khklfa.tasktracker.service.UserService;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final AuthDataHolderInSession authDataHolder;

    private final UserDaoService userDaoService;

    private final UserEntityMapper userEntityMapper;

    @Override
    public void register(RegisterDto registerDto) {
        if (!userDaoService.existByLogin(registerDto.getLogin())) {
            User user = userEntityMapper.toEntity(registerDto)
                    .setRoles(Collections.singleton(Role.USER));
            userDaoService.save(user);
        }
    }

    @Override
    public UserViewDto getAuthenticatedUser() {
        return authDataHolder.getUser();
    }

    @Override
    public List<UserViewDto> getAll() {
        List<User> allUsers = userDaoService.getAllUsers();
        return userEntityMapper.toViewDto(allUsers);
    }

    @Override
    public List<UserViewDto> getAllExceptCurrent() {
        List<User> allUsers = userDaoService.getAllUsers().stream()
                .filter(user -> !user.getLogin().equals(getAuthenticatedUser().getLogin()))
                .collect(Collectors.toList());
        return userEntityMapper.toViewDto(allUsers);
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        User user = userDaoService.findByLogin(username);
        return userEntityMapper.toViewDto(user);
    }
}
