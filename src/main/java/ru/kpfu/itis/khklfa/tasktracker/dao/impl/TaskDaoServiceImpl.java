package ru.kpfu.itis.khklfa.tasktracker.dao.impl;

import liquibase.util.StringUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kpfu.itis.khklfa.tasktracker.dao.TaskDaoService;
import ru.kpfu.itis.khklfa.tasktracker.dto.model.SearchDto;
import ru.kpfu.itis.khklfa.tasktracker.entity.Task;
import ru.kpfu.itis.khklfa.tasktracker.entity.enums.TaskStatus;
import ru.kpfu.itis.khklfa.tasktracker.exceptions.NotFoundInDbException;
import ru.kpfu.itis.khklfa.tasktracker.repository.TaskRepository;
import ru.kpfu.itis.khklfa.tasktracker.spec.TaskSpec;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class TaskDaoServiceImpl implements TaskDaoService {

    private final TaskRepository taskRepository;

    @Override
    @Transactional
    public Task createTask(Task task) {
        return taskRepository.save(task);
    }

    @Override
    @Transactional(readOnly = true)
    public Task findById(UUID taskId) {
        return taskRepository.findById(taskId)
                .orElseThrow(NotFoundInDbException::new);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Task> getUserTaskListByUserName(String userName) {
        return taskRepository.findAllByExecutor_Login(userName);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Task> getAll() {
        return taskRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public List<Task> getUserTaskListByUserNameAndStatus(String userName, TaskStatus taskStatus) {
        return taskRepository.findAllByExecutor_LoginAndTaskStatus(userName, taskStatus);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Task> getTaskListBySearchDtoAndTaskStatus(SearchDto searchDto, TaskStatus taskStatus) {
        Specification<Task> spec = TaskSpec.getTaskStatusSpec(taskStatus);
        if (StringUtils.isNotEmpty(searchDto.getExecutorLogin())) {
            spec = spec.and(TaskSpec.getExecutorSpec(searchDto.getExecutorLogin()));
        }
        if (StringUtils.isNotEmpty(searchDto.getFrom()) && StringUtils.isNotEmpty(searchDto.getTo())) {
            spec = spec.and(TaskSpec.getDeadlineSpec(LocalDate.parse(searchDto.getFrom()), LocalDate.parse(searchDto.getTo())));
        }

        return taskRepository.findAll(spec);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Task> getChildrenTasks(UUID taskId) {
        return taskRepository.findAllByParentId(taskId);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Task> getUserTaskListByStatus(TaskStatus taskStatus) {
        return taskRepository.findAll(TaskSpec.getTaskStatusSpec(taskStatus));
    }

    /**
     * Добавлено после презентации.
     * Доработка: Отображать в профиле пользователя созданные задания
     */
    @Override
    @Transactional(readOnly = true)
    public List<Task> getUserTaskListByCreatedUserName(String userName) {
        return taskRepository.findAllByCreator_Login(userName);
    }
}
